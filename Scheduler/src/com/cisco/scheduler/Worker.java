package com.cisco.scheduler;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Worker {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;

	public void send() throws AddressException, MessagingException {
		final String username = "gocodea@gmail.com";
		final String password = "gocode12345";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("gocodea@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("gocodea@gmail.com"));
			message.setSubject("The prices have fallen!");
			message.setText("Dear Sir/Madam,"
					+ "\n\nThe price for the flight you were looking "
					+ "for has come down and is now within your budget! "
					+ "Kindly login to www.goibibo.com to book your tickets now.");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void run(String data){
		try {
			send();
		} catch (AddressException e) {			
			e.printStackTrace();
		} catch (MessagingException e) {			
			e.printStackTrace();
		}
	}
}

package com.cisco.scheduler;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class customJob implements Job
{
	public void execute(JobExecutionContext context)
	throws JobExecutionException {
		
		System.out.println("Creating Worker object");
		JobDataMap data = context.getJobDetail().getJobDataMap();		
		
		Worker worker = new Worker();
		worker.run(data.getString("data"));
		
		System.out.println("Email sent at "+ new Date(System.currentTimeMillis()));
	
	}
	
}
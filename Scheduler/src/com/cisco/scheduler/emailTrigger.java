package com.cisco.scheduler;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

public class emailTrigger {

	public void schedule(String jsonData) throws Exception{
		
		String source, destination, startDateS, endDateS, classType;
		Date startDate, endDate;
		
		SimpleDateFormat myFormat = new SimpleDateFormat("yyyyMMdd");
		
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonData);
		
		source = document.getString("source");
		destination = document.getString("destn");
		startDateS = document.getString("start");
		endDateS = document.getString("end");
		classType = document.getString("class");
		
		startDate = myFormat.parse(startDateS);
		endDate = myFormat.parse(endDateS);
		
		JobDetail job = JobBuilder.newJob(customJob.class).withIdentity("email").build();
		
		job.getJobDataMap().put("data", jsonData);
		job.getJobDataMap().put("source", source);
		job.getJobDataMap().put("destn", destination);
		job.getJobDataMap().put("start", startDateS);
		job.getJobDataMap().put("end", endDateS);
		job.getJobDataMap().put("class", classType);
		
		Trigger trigger = TriggerBuilder.newTrigger()
				.startAt(startDate)
				.endAt(endDate)
				.withIdentity("email")
				.withSchedule(
						SimpleScheduleBuilder.simpleSchedule()
						.withIntervalInMinutes(2).withRepeatCount(2))
				.build();
		
		// schedule it
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
		scheduler.scheduleJob(job, trigger);
		
	}
	
}

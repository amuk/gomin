package com.cisco.scheduler;

import java.net.UnknownHostException;
import com.mongodb.DB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class gominAction {
	
	public DB initialize() {

		final MongoClient mongoClient;
		final DB ECDatabase;

		try {
			mongoClient = new MongoClient(new MongoClientURI(
					"mongodb://localhost"));
			ECDatabase = mongoClient.getDB("gomin");	
			
			System.out.println("Initialized");
			return ECDatabase;
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		}
		return null;
	}
}

package com.cisco.scheduler;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.util.JSON;

public class restCallDAO {
	DBCollection userdataCollection;
	
	public void restCallDAO(final DB gominDatabase){
		userdataCollection = gominDatabase.getCollection("users");
	}	
	
	public String makeRestCall(String jsonData) throws ClientProtocolException,
			IOException {
		
		String source, destination, startDate, endDate, classType, output;
		System.out.println(jsonData);
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonData);
		
		source = document.getString("source");
		destination = document.getString("destn");
		startDate = document.getString("start");
		endDate = document.getString("end");
		classType = document.getString("class");
		
		String str = "http://developer.goibibo.com/api/stats/minfare/?"
				+ "app_id=3ec08889&app_key=5ed83892dd5fbd110dce81702e20edbf&"
				+ "format=json&vertical=flight&source=";
		str = str.concat(source);
		str = str.concat("&destination=");
		str = str.concat(destination);
		str = str.concat("&mode=one&sdate=");
		str = str.concat(startDate);
		str = str.concat("&edate=");
		str = str.concat(endDate);
		str = str.concat("&class=");
		str = str.concat(classType);
		
		try {
			 
			Client client = Client.create();
			WebResource webResource2 = client.resource(str);
			ClientResponse response2 = webResource2.accept("application/json").get(ClientResponse.class);
			if (response2.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response2.getStatus());
			}
 
			output = response2.getEntity(String.class);			
			System.out.println(output);
 
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		return output;
	}	
	
}
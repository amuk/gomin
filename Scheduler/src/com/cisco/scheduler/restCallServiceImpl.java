package com.cisco.scheduler;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.mongodb.DB;

@Path("/minFareService")
public class restCallServiceImpl {

	@POST
	@Path("/checkMinFare")
	@Produces("application/json")
	public String checkMinFare(String jsonData) throws Exception {			
		String output;		
		gominAction ecaction = new gominAction();
		DB ECDatabase = ecaction.initialize();
		
		emailTrigger trigger = new emailTrigger();		
		restCallDAO restcalldao = new restCallDAO();	
		
		try {				
			output = restcalldao.makeRestCall(jsonData);
			trigger.schedule(jsonData);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Error returned.";
	}
	
	@POST
	@Path("/checkCancellation")
	@Produces("application/json")
	public String checkCancellation(String jsonData) throws Exception {			
		String output;		
		emailTrigger trigger = new emailTrigger();		
		restCallDAO restcalldao = new restCallDAO();	
		
		try {				
			output = restcalldao.makeRestCall(jsonData);
			trigger.schedule(jsonData);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Error returned.";
	}
	
}

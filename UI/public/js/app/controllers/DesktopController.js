define(['App', 'backbone', 'marionette', 'views/WelcomeView', 'views/DesktopHeaderView',"jquery.fullPage"],
    function (App, Backbone, Marionette, WelcomeView, DesktopHeaderView,fullPage) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {

            //App.headerRegion.show(new DesktopHeaderView());
        },
        //gets mapped to in AppRouter's appRoutes
        index:function () {
            App.mainRegion.show(new WelcomeView());
        },

        start: function(){
            App.mainRegion.show(new WelcomeView());
        },

        secondPage: function(){
            App.mainRegion.show(new WelcomeView());
        },

        thirdPage: function(){

            App.mainRegion.show(new WelcomeView());
        },
        login: function(){
            App.mainRegion.show(new WelcomeView());
        $("#loginForm").remove();
            $("#signup").remove();
         $("#menu").append('<li class="pull-right"><a href="/logout">Logout</a></li>');
        }
    });
});
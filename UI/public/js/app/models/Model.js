define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {

            },

            // Default values for all of the Model attributes
            defaults: function(){
                return {
                    'useremail': null,
                    'destination':null,
                    "source":null,
                    "budget":null,
                    "checkCancellations":null,
                    "startDate":null,
                    "returnDate": null,
                    "hotelNeeded": null
                };
            },

            // Get's called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;

    }

);
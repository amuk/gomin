define(['App','backbone', 'marionette'], function(App, Backbone, Marionette) {
    var AppService = {
        init: function() {
            _.bindAll(this);

            //Sets the Request Response Handler.
            App.reqres.setHandlers({
                "details:search": function(model) {
                    return AppService.searchFlights(model);
                }
            });
        },
        searchFlights : function(model){
            if(model.get("checkCancellation")){
                model.url = "http://localhost:8080/Scheduler/rest/minFareService/checkCancellation";
            }else{
                model.url = "http://localhost:8080/Scheduler/rest/minFareService/checkMinFare";
            }
            Backbone.sync("create", model, {
                success: function(model, response) {
                    if (response.status === 'ERROR') {
                       return false;
                    } else {
                        return true;
                    }
                },
                error: function(model, response) {
                    return false;
                }
            });
            return false;
        }
    };
    return AppService;
});